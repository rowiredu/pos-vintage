<div class="container">
   <div class="row" style="margin-top:60px;">
      <div class="col-md-4">
         <div class="statCart Statcolor04">
        <i class="fa fa-envelope" aria-hidden="true"></i>
        <h1>email</h1><br>
        <span>info@zentechgh.com</span>
        </div>
      </div>
      <div class="col-md-4">
         <div class="statCart Statcolor04">
        <i class="fa fa-phone" aria-hidden="true"></i>
        <h1>call</h1><br>
        <span>+(233) 0501338822)</span>
        </div>
      </div>
      <div class="col-md-4">
         <div class="statCart Statcolor04">
        <i class="fa fa-location-arrow" aria-hidden="true"></i>
        <h2 style="display: inline">Locate Us</h2><br>
        <span>Tesano Commesal plaza</span>
        </div>
      </div>
   </div>
   <div class="row" style="margin-top:50px;">
      <div class="col-md-12">
         <!-- chart container  -->
         <div class="statCart">
            <h3>Map</h3>
            <div style="width:100%">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3970.7900733842453!2d-0.23221066402554177!3d5.598004123924223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcc62a0a827e09f0c!2sZentech+IT+Solutions+Limited!5e0!3m2!1sen!2sgh!4v1483730058948" height="400" frameborder="0" style="border:0; width: 100%" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>

   <div class="row" style="margin-top:60px;">
      <div class="col-md-12">
         <div class="statCart Statcolor04">
        <i class="fa fa-globe" aria-hidden="true"></i>
        <h2 style="display: inline">website</h2><br>
        <span><a href="http://zentechgh.com/" style="color: #3fc4dd;"><?=$this->setting->companyname?>  >> www.zentechgh.com << </a></span>
        </div>
      </div>
   </div>